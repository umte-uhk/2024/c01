package cz.uhk.c01.ui

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import cz.uhk.c01.ui.alerts.AlertsScreen
import cz.uhk.c01.ui.datastore.DataStoreScreen
import cz.uhk.c01.ui.detail.RocketDetailScreen
import cz.uhk.c01.ui.home.HomeScreen
import cz.uhk.c01.ui.launches.RocketLaunchesScreen
import cz.uhk.c01.ui.notes.NotesScreen

@Composable
fun AppContainer() {
    val controller = rememberNavController()
    NavHost(
        navController = controller,
        startDestination = DestinationHome,
    ) {
        composable(DestinationHome) {
            HomeScreen(
                navigateLaunches = {
                    controller.navigate(DestinationLaunches)
                },
                navigateNotes = {
                    controller.navigate(DestinationNotes)
                },
                navigateDataStore = {
                    controller.navigate(DestinationDataStore)
                },
                navigateAlerts = {
                    controller.navigate(DestinationAlerts)
                }
            )
        }
        composable(DestinationLaunches) {
            RocketLaunchesScreen(
                onNavigateDetail = { rocketId ->
                    controller.navigate(DestinationRocketDetail.replace("{rocketId}", rocketId))
                }
            )
        }
        composable(
            route = DestinationRocketDetail,
            arguments = listOf(navArgument("rocketId") { type = NavType.StringType }),
        ) { entry ->
            RocketDetailScreen(
                rocketId = entry.arguments?.getString("rocketId").orEmpty(),
            )
        }
        composable(DestinationNotes) {
            NotesScreen()
        }
        composable(DestinationDataStore) {
            DataStoreScreen()
        }
        composable(DestinationAlerts) {
            AlertsScreen()
        }
    }
}

private const val DestinationHome = "home"
private const val DestinationLaunches = "launches"
private const val DestinationRocketDetail = "rocket/{rocketId}"
private const val DestinationNotes = "notes"
private const val DestinationDataStore = "data store"
private const val DestinationAlerts = "alerts"