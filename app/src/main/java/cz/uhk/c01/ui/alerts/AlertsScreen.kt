package cz.uhk.c01.ui.alerts

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import cz.uhk.c01.R

@Composable
fun AlertsScreen() {
    val context = LocalContext.current
    val permissionGranted = remember {
        mutableStateOf(context.isAlertsGranted())
    }
    val launcher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestPermission(),
        onResult = { granted ->
            Toast.makeText(context, if (granted) "Povoleno" else "Nepovoleno", Toast.LENGTH_SHORT)
                .show()
            permissionGranted.value = granted
        }
    )

    val takePhoto = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.TakePicturePreview(),
        onResult = { bitmap ->
            println(bitmap)
        }
    )

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
    ) {
        if (permissionGranted.value.not()) {
            Button(
                onClick = {
                    launcher.launch(Manifest.permission.POST_NOTIFICATIONS)
                },
            ) {
                Text(text = "Získat oprávnění")
            }
        } else {
            Button(onClick = {
                // HOW TO TAKE PHOTO: takePhoto.launch(null)
                sendAlert(context)
            }) {
                Text(text = "Odeslat notifikaci")
            }
        }
    }
}

private fun sendAlert(context: Context) {
    val channelId = "General"
    val builder = NotificationCompat.Builder(context, channelId)
        .setSmallIcon(R.drawable.ic_launcher_foreground)
        .setContentTitle("Nadpis notifikace")
        .setContentText("Tělo / zpráva notifikace")
        .setPriority(NotificationCompat.PRIORITY_DEFAULT)

    val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        manager.createNotificationChannel(
            NotificationChannel(channelId, "Obecné", NotificationManager.IMPORTANCE_DEFAULT)
        )
    }

    manager.notify(1, builder.build())
}

private fun Context.isAlertsGranted() =
    ActivityCompat.checkSelfPermission(
        this, Manifest.permission.POST_NOTIFICATIONS
    ) == PackageManager.PERMISSION_GRANTED