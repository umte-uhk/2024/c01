package cz.uhk.c01.ui.notes

import cz.uhk.c01.data.db.daos.NoteDao
import cz.uhk.c01.data.db.entities.NoteEntity
import cz.uhk.c01.ui.base.BaseViewModel

class NotesViewModel(
    private val noteDao: NoteDao,
) : BaseViewModel() {

    val notes = noteDao.selectAll()

    fun addNote(text: String) {
        launch {
            noteDao.insertOrUpdate(
                note = NoteEntity(
                    text = text,
                )
            )
        }
    }

    fun handleNoteSolved(note: NoteEntity) {
        launch {
            noteDao.insertOrUpdate(
                note = note.copy(
                    solved = note.solved.not(),
                )
            )
        }
    }

    fun handleNotePriority(note: NoteEntity, newPriority: Int) {
        launch {
            noteDao.insertOrUpdate(
                note = note.copy(
                    priority = newPriority,
                )
            )
        }
    }
}