@file:OptIn(ExperimentalMaterial3Api::class)

package cz.uhk.c01.ui.form

import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.RadioButton
import androidx.compose.material3.RadioButtonDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

enum class Gender(val title: String) {
    Man("Muž"),
    Woman("Žena"),
    Undefined("Nevybráno"),
}

data class Person(
    val name: String,
)

@Preview
@Composable
fun FormScreen() {
    val context = LocalContext.current

    val inputName = remember { mutableStateOf<String>("") }
    val inputSurname = remember { mutableStateOf("") }
    val inputGender = remember { mutableStateOf(Gender.Undefined) }
    val inputPerson = remember { mutableStateOf<Person?>(null) }

    val color = animateColorAsState(
        label = "Color",
        targetValue = when (inputGender.value) {
            Gender.Man -> Color.Cyan
            Gender.Woman -> Color.Blue
            Gender.Undefined -> Color.Gray
        },
    )

    val personName = inputPerson.value?.name ?: "Není vyplněno"

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = "Formulář") },
                navigationIcon = {
                    IconButton(onClick = { (context as FormActivity).finish() }) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            contentDescription = "Zpět",
                        )
                    }
                },
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primary,
                    titleContentColor = MaterialTheme.colorScheme.onPrimary,
                    navigationIconContentColor = MaterialTheme.colorScheme.onPrimary,
                ),
            )
        },
    ) { paddingValues ->
        Column(
            modifier = Modifier
                .padding(
                    top = paddingValues.calculateTopPadding(),
                    bottom = paddingValues.calculateBottomPadding(),
                )
                .padding(16.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp),
        ) {
            Text(text = "Vyplňte formulář")
            OutlinedTextField(
                label = { Text(text = "Jméno") },
                value = inputName.value,
                onValueChange = { inputName.value = it },
                modifier = Modifier.fillMaxWidth(),
                singleLine = true,
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Text,
                    imeAction = ImeAction.Next,
                )
            )
            OutlinedTextField(
                label = { Text(text = "Přijmení") },
                value = inputSurname.value,
                onValueChange = { inputSurname.value = it },
                modifier = Modifier.fillMaxWidth(),
                singleLine = true,
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Text,
                    imeAction = ImeAction.Done,
                )
            )
            Row {
                Gender.entries.forEach { gender ->
                    SingleRadioButton(
                        color = color.value,
                        gender = gender,
                        selectedGender = inputGender.value,
                        onGenderSelected = { inputGender.value = it },
                        //onLog = { gender.title },
                    )
                }
            }
        }
    }
}

@Composable
private fun SingleRadioButton(
    color: Color,
    gender: Gender,
    selectedGender: Gender,
    onGenderSelected: (Gender) -> Unit,
    //onLog: () -> String,
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
    ) {
        RadioButton(
            selected = gender == selectedGender,
            onClick = {
                onGenderSelected(gender)
                //println(onLog())
            },
            colors = RadioButtonDefaults.colors(
                selectedColor = color,
            ),
        )
        Text(
            text = gender.title,
        )
    }
}

data class RadioData(
    val gender: Gender,
    val selectedGender: Gender,
    val onGenderSelected: (Gender) -> Unit,
    val onLog: () -> String,
)