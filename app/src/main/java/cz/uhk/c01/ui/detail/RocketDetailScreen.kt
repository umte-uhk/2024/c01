package cz.uhk.c01.ui.detail

import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import org.koin.androidx.compose.getViewModel
import org.koin.core.parameter.parametersOf

@Composable
fun RocketDetailScreen(
    rocketId: String,
    viewModel: RocketDetailViewModel = getViewModel {
        parametersOf(rocketId)
    },
) {

    val detail = viewModel.detail.collectAsState()

    // TODO when for state UI

    detail.value?.let { detail ->
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
        ) {
            Text(text = detail.name, style = MaterialTheme.typography.headlineLarge)
            Text(text = detail.description)
            detail.wikipedia?.let { wiki ->
                val context = LocalContext.current
                Button(
                    onClick = {
                        context.startActivity(
                            Intent(Intent.ACTION_VIEW, Uri.parse(wiki))
                        )
                    },
                ) {
                    Text(text = "Otevřít na Wiki")
                }
            }
        }
    }
}