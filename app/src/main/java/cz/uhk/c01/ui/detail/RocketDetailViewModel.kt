package cz.uhk.c01.ui.detail

import cz.uhk.c01.data.RocketDetailResponse
import cz.uhk.c01.di.SpaceXRepository
import cz.uhk.c01.ui.base.BaseViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class RocketDetailViewModel(
    private val rocketId: String,
    private val repo: SpaceXRepository,
) : BaseViewModel() {

    private val _detail = MutableStateFlow<RocketDetailResponse?>(null)
    val detail = _detail.asStateFlow()

    init {
        launch {
            repo.fetchRocketDetail(rocketId)
                .also {
                    _detail.emit(it)
                }
        }
    }
}