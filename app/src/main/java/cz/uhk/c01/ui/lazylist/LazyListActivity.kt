package cz.uhk.c01.ui.lazylist

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Build
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.outlined.ThumbUp
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import cz.uhk.c01.ui.theme.UMTEAppTheme

class LazyListActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ListOfPeople()
        }
    }
}

@Composable
fun ListOfPeople() {
    UMTEAppTheme {

        val people = remember {
            generatePeople(count = 200)
        }

        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .background(MaterialTheme.colorScheme.background),
            content = {
                items(people) { human ->
                    Card {
                        Row(
                            modifier = Modifier.padding(16.dp),
                            verticalAlignment = Alignment.CenterVertically,
                        ) {
                            Text(
                                text = human.name,
                                style = MaterialTheme.typography.headlineMedium,
                                modifier = Modifier.weight(1F),
                            )
                            Icon(
                                imageVector = human.status.icon(),
                                contentDescription = null,
                            )
                        }
                    }
                }
            },
        )
    }
}

private val names = arrayOf("John", "Tomáš", "Petr", "Filip")
private val surnames = arrayOf("Novák", "Kozel", "Pražák", "Malý")

private fun generatePeople(count: Int = 100) =
    mutableListOf<Human>().apply {
        // for (i in 0..count) { }
        repeat(count) {
            add(
                Human(
                    name = "${names.random()} ${surnames.random()}",
                    status = Status.entries.random(),
                )
            )
        }
    }

data class Human(
    val name: String,
    val status: Status,
)

enum class Status {

    Okay, Favorite, Unspecified;

    fun icon() = when (this) {
        Okay -> Icons.Outlined.ThumbUp
        Favorite -> Icons.Filled.Check
        Unspecified -> Icons.Default.Build
    }
}