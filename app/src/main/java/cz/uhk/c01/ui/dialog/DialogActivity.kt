package cz.uhk.c01.ui.dialog

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import cz.uhk.c01.ui.theme.UMTEAppTheme

class DialogActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            UMTEAppTheme {
                AlertDialogScreen()
            }
        }
    }
}

@Composable
fun AlertDialogScreen() {
    val dialogShown = remember {
        mutableStateOf(false)
    }
    Button(onClick = { dialogShown.switch() }) {
        Text(
            text = if (dialogShown.value) "Zavřít"
            else "Otevřít",
        )
    }
    if (dialogShown.value) {
        AlertDialog(
            onDismissRequest = { dialogShown.switch() },
            title = { Text("Titulek") },
            text = { Text("Obsah dialogu...") },
            confirmButton = {
                TextButton(
                    onClick = { dialogShown.switch() },
                ) {
                    Text(text = "Zavřít")
                }
            },
        )
    }
}

fun MutableState<Boolean>.switch() {
    value = value.not()
}