@file:OptIn(ExperimentalFoundationApi::class)

package cz.uhk.c01.ui.notes

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AddCircle
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.Button
import androidx.compose.material3.Checkbox
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import org.koin.androidx.compose.getViewModel

@Composable
fun NotesScreen(
    viewModel: NotesViewModel = getViewModel(),
) {

    val notes = viewModel.notes.collectAsState(emptyList())

    Column {

        LazyColumn(
            modifier = Modifier
                .weight(1F)
                .fillMaxWidth(),
            contentPadding = PaddingValues(16.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp),
        ) {
            items(
                items = notes.value,
                key = { it.id },
            ) { note ->
                ElevatedCard(
                    modifier = Modifier
                        .fillMaxWidth()
                        .animateItemPlacement()
                ) {
                    Column(
                        modifier = Modifier.padding(16.dp),
                    ) {
                        Text(
                            text = note.text,
                            style = MaterialTheme.typography.bodyMedium,
                        )
                        Row {
                            IconButton(
                                onClick = {
                                    viewModel.handleNotePriority(
                                        note = note,
                                        newPriority = note.priority + 1
                                    )
                                },
                            ) {
                                Icon(
                                    imageVector = Icons.Default.AddCircle,
                                    contentDescription = null,
                                )
                            }
                            IconButton(
                                onClick = {
                                    viewModel.handleNotePriority(
                                        note = note,
                                        newPriority = note.priority - 1
                                    )
                                },
                            ) {
                                Icon(
                                    imageVector = Icons.Default.ArrowDropDown,
                                    contentDescription = null,
                                )
                            }
                        }
                        Checkbox(
                            checked = note.solved,
                            onCheckedChange = { viewModel.handleNoteSolved(note) },
                        )
                    }
                }
            }
        }

        val inputText = remember { mutableStateOf("") }
        Row(
            modifier = Modifier.padding(16.dp),
            horizontalArrangement = Arrangement.spacedBy(8.dp),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            OutlinedTextField(
                modifier = Modifier.weight(1F),
                label = { Text(text = "Úkol") },
                value = inputText.value,
                onValueChange = { inputText.value = it },
            )
            Button(
                modifier = Modifier.height(IntrinsicSize.Max),
                shape = RoundedCornerShape(4.dp),
                enabled = inputText.value.isNotBlank(),
                onClick = {
                    viewModel.addNote(inputText.value)
                    inputText.value = ""
                },
            ) {
                Text(text = "Uložit")
            }
        }
    }
}