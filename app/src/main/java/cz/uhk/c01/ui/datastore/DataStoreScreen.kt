package cz.uhk.c01.ui.datastore

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Checkbox
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import org.koin.androidx.compose.getViewModel

@Composable
fun DataStoreScreen(
    viewModel: DataStoreViewModel = getViewModel(),
) {

    val lastText = viewModel.textFlow.collectAsState(initial = "")

    var inputText by remember { mutableStateOf("") }
    var checkBox by remember { mutableStateOf(false) }

    LaunchedEffect(key1 = "InitData") {
        checkBox = viewModel.getInitChecked()
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        verticalArrangement = Arrangement.spacedBy(16.dp),
    ) {
        OutlinedTextField(
            value = inputText,
            onValueChange = {
                inputText = it
            },
        )
        Text(text = "Poslední známá hodnota:")
        Text(text = lastText.value)

        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(16.dp),
        ) {
            Checkbox(
                checked = checkBox,
                onCheckedChange = { checkBox = it },
            )
            Text(text = "Checked")
        }

        Button(
            onClick = {
                viewModel.save(inputText, checkBox)
            },
        ) {
            Text(text = "Uložit")
        }
    }
}