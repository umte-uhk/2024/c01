package cz.uhk.c01.ui.datastore

import cz.uhk.c01.data.datastore.Storage
import cz.uhk.c01.ui.base.BaseViewModel

class DataStoreViewModel(
    private val storage: Storage,
) : BaseViewModel() {

    val textFlow = storage.textFlow

    fun save(text: String, checked: Boolean) {
        launch {
            storage.setText(text)
            storage.setChecked(checked)
        }
    }

    suspend fun getInitChecked() = storage.isChecked()
}