package cz.uhk.c01.ui.home

import android.content.Intent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import cz.uhk.c01.ui.dialog.DialogActivity
import cz.uhk.c01.ui.form.FormActivity
import cz.uhk.c01.ui.lazylist.LazyListActivity

@Composable
fun HomeScreen(
    navigateLaunches: () -> Unit,
    navigateNotes: () -> Unit,
    navigateDataStore: () -> Unit,
    navigateAlerts: () -> Unit,
) {

    val context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
    ) {
        Button(
            onClick = {
                context.startActivity(
                    Intent(context, FormActivity::class.java)
                )
            },
            content = {
                Text(text = "Formulář")
            },
        )
        Button(
            onClick = {
                context.startActivity(
                    Intent(context, LazyListActivity::class.java)
                )
            },
        ) {
            Text(text = "Lazy list")
        }

        Button(
            onClick = {
                context.startActivity(
                    Intent(context, DialogActivity::class.java)
                )
            },
        ) {
            Text(text = "Dialog")
        }

        Button(
            onClick = { navigateLaunches() },
        ) {
            Text(text = "Launches")
        }

        Button(
            onClick = { navigateNotes() },
        ) {
            Text(text = "Poznámky")
        }

        Button(
            onClick = { navigateDataStore() },
        ) {
            Text(text = "Data store")
        }

        Button(
            onClick = { navigateAlerts() },
        ) {
            Text(text = "Notifikace")
        }
    }
}