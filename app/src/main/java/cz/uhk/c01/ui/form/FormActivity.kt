package cz.uhk.c01.ui.form

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import cz.uhk.c01.ui.theme.UMTEAppTheme

class FormActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            UMTEAppTheme {
                FormScreen()
            }
        }
    }
}