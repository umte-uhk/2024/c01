package cz.uhk.c01.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.room.Room
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import cz.uhk.c01.data.SpaceXApi
import cz.uhk.c01.data.datastore.Storage
import cz.uhk.c01.data.db.AppDatabase
import cz.uhk.c01.ui.datastore.DataStoreViewModel
import cz.uhk.c01.ui.detail.RocketDetailViewModel
import cz.uhk.c01.ui.launches.RocketLaunchesViewModel
import cz.uhk.c01.ui.notes.NotesViewModel
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit


val appModules by lazy { listOf(dataModule, uiModule) }


val dataModule = module {
    repositories()
    api()
    db()
    dataStore()
}

val uiModule = module {
    viewModel { RocketLaunchesViewModel(get()) }
    viewModel { (rocketId: String) -> RocketDetailViewModel(rocketId, get()) }
    viewModel { NotesViewModel(get()) }
    viewModel { DataStoreViewModel(get()) }
}


private fun Module.repositories() {
    single { SpaceXRepository(get()) }
}

private fun Module.api() {
    single { createRetrofit() }
    single { get<Retrofit>().create(SpaceXApi::class.java) } // SpaceXAPI
}

private fun Module.db() {
    // DB
    single {
        Room.databaseBuilder(
            context = androidApplication(),
            klass = AppDatabase::class.java,
            name = AppDatabase.Name,
        ).build()
    }
    // DAO
    single {
        get<AppDatabase>().noteDao()
    }
}

private fun Module.dataStore() {
    single {
        Storage(androidApplication().dataStore)
    }
}

private const val DataStoreName = "UMTEDataStore"
private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(DataStoreName)


private val json = Json {
    ignoreUnknownKeys = true
}

private fun createRetrofit(
    client: OkHttpClient = createClient(),
    baseURL: String = BaseApiURL,
) = Retrofit.Builder()
    .client(client)
    .baseUrl(baseURL)
    .addConverterFactory(
        json.asConverterFactory(
            MediaType.get("application/json")
        )
    )
    .build()

private fun createClient() = OkHttpClient.Builder().build()

private const val BaseApiURL = "https://api.spacexdata.com/v3/" // launches