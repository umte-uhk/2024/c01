package cz.uhk.c01.di

import cz.uhk.c01.data.SpaceXApi

class SpaceXRepository(
    private val api: SpaceXApi,
) {

    suspend fun fetchAllSuccessfulLaunches() =
        api.fetchLaunches()

    suspend fun fetchRocketDetail(id: String) =
        api.fetchRocketDetail(id)
}