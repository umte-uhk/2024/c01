package cz.uhk.c01.data.datastore

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking

class Storage(
    private val dataStore: DataStore<Preferences>,
) {

    val textFlow: Flow<String> = dataStore.data
        .map { preferences -> preferences[TextDataKey].orEmpty() }

    suspend fun isChecked(): Boolean = dataStore.data
        .map { it[CheckDataKey] }.first() ?: false

    suspend fun setText(value: String) = dataStore.edit {
        it[TextDataKey] = value
    }

    suspend fun setChecked(value: Boolean) = dataStore.edit { prefs ->
        prefs[CheckDataKey] = value
    }


    companion object {
        private val TextDataKey = stringPreferencesKey("TextDataKey")
        private val CheckDataKey = booleanPreferencesKey("CheckDataKey")
    }
}



