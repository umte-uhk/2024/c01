package cz.uhk.c01.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class LaunchResponse(

    @SerialName("flight_number")
    val flightNumber: Int,

    @SerialName("mission_name")
    val missionName: String,

    val rocket: RocketResponse,
)

@Serializable
data class RocketResponse(

    @SerialName("rocket_id")
    val id: String,

    @SerialName("rocket_name")
    val name: String,
)
