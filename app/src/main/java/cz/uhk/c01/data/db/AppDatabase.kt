package cz.uhk.c01.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import cz.uhk.c01.data.db.daos.NoteDao
import cz.uhk.c01.data.db.entities.NoteEntity

@Database(
    version = AppDatabase.Version,
    entities = [ NoteEntity::class ],
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun noteDao(): NoteDao

    companion object {
        const val Version = 1
        const val Name = "Umte-db"
    }
}