package cz.uhk.c01.data.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import cz.uhk.c01.data.db.entities.NoteEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface NoteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrUpdate(note: NoteEntity)

    // Select * From NoteEntity Where Not(solved) Order by priority Desc
    @Query("Select * From NoteEntity Order by priority Desc")
    fun selectAll(): Flow<List<NoteEntity>>
}