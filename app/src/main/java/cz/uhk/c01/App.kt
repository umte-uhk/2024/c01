package cz.uhk.c01

import android.app.Application
import cz.uhk.c01.di.appModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(applicationContext)
            androidLogger(
                Level.DEBUG
                // TODO if (BuildConfig.DEBUG)
                // TODO module
            )
            modules(appModules)
        }
    }
}